# -*- coding: utf-8 -*-
import os

import click
from flask.cli import with_appcontext
from flask_security.utils import hash_password

from application.extensions import db

@click.command()
@with_appcontext
def create_db():
    db.create_all()
    print('Database structure created successfully')



@click.command()
@with_appcontext
def install():
    from application.user.models import Role, User
    a = Role.query.filter(Role.name == 'admin').first()
    try:
        if a is None:
            a = Role(name='admin')
            db.session.add(a)
            db.session.commit()
        u = click.prompt('Admin Email?', default='yasar@dijitalguru.com')
        p = click.prompt('Admin Password (min 6 characters)?', default='dijitalguru')
        user = User(email=u, password=hash_password(p), active=1)
        user.roles.append(a)
        user.save()
    except Exception as e:
        db.session.rollback()


@click.command()
@click.option('-e', '--email', prompt=True, default=None)
@click.option('-p', '--password', prompt=True, default=None)
@with_appcontext
def create(email, password):
    from application.user.models import User
    a = User.query.filter(User.email == email).first()
    if a is not None:
        print('User already exists!')
    else:
        user = User(email=email, password=hash_password(password), active=1)
        user.save()


@click.command()
@click.option('-e', '--email', prompt=True, default=None)
@click.option('-r', '--role', prompt=True, default='admin')
@with_appcontext
def add_role(email, role):
    from application.user.models import Role, User
    u = User.query.filter(User.email == email).first()
    if u is None:
        print('Sorry, this user does not exist!')
    else:
        r = Role.query.filter(Role.name == role).first()
        if r is None:
            print('Sorry, this role does not exist!')
            u = click.prompt('Would you like to create one? Y/N', default='N')
            if u.lower() == 'y':
                r = Role(name=role)
                try:
                    db.session.add(r)
                    db.session.commit()
                    print('Role created successfully, you may add it now to the user')
                except Exception as e:
                    db.session.rollback()
        # add role to user
        u.roles.append(r)


@click.command()
@click.option('-e', '--email', prompt=True, default=None)
@click.option('-p', '--password', hide_input=True, confirmation_prompt=True, prompt=True, default=None)
@with_appcontext
def reset(email, password):
    from application.user.models import User
    try:
        pwd = hash_password(password)
        u = User.query.filter(User.email == email).first()
        u.password = pwd
        try:
            db.session.commit()
            print('User password has been reset successfully.')
        except:
            db.session.rollback()
    except Exception as e:
        print('Error resetting user password: %s' % e)


@click.command()
def clean():
    for dir_path, dir_names, file_names in os.walk('.'):
        for filename in file_names:
            if filename.endswith('.pyc') or filename.endswith('.pyo'):
                full_pathname = os.path.join(dir_path, filename)
                click.echo('Removing {}'.format(full_pathname))
                os.remove(full_pathname)
