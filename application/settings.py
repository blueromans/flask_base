# -*- coding: utf-8 -*-
import os, redis

from dotenv import find_dotenv, load_dotenv

os_env = os.environ
ENV_FILE = find_dotenv()
if ENV_FILE:
    load_dotenv(ENV_FILE)


class Config(object):
    SECRET_KEY = '3nF3Rn0'
    APP_DIR = os.path.abspath(os.path.dirname(__file__))  # This directory
    PROJECT_ROOT = os.path.abspath(os.path.join(APP_DIR, os.pardir))
    DEBUG_TB_ENABLED = False
    DEBUG_TB_INTERCEPT_REDIRECTS = False
    CACHE_TYPE = 'simple'
    SQLALCHEMY_DATABASE_URI = 'postgresql://{0}:{1}@{2}:{3}/{4}'. \
        format(os.environ.get('SQLALCHEMY_USER'),
               os.environ.get('SQLALCHEMY_PASSWORD'),
               os.environ.get('SQLALCHEMY_HOST'),
               os.environ.get('SQLALCHEMY_PORT'),
               os.environ.get('SQLALCHEMY_DB'))
    SQLALCHEMY_TRACK_MODIFICATIONS = True

    CELERY_BROKER_URL = os.environ.get('CELERY_BROKER_URL',
                                       'redis://:{0}@localhost:6379/10'.format(os.environ.get('REDIS_PASS')))
    CELERY_RESULT_BACKEND = os.environ.get('CELERY_RESULT_BACKEND',
                                           'redis://:{0}@localhost:6379/11'.format(os.environ.get('REDIS_PASS')))

    # security
    SECURITY_REGISTERABLE = True
    SECURITY_RECOVERABLE = True
    SECURITY_CONFIRMABLE = False
    SECURITY_TRACKABLE = True
    SECURITY_PASSWORD_HASH = 'bcrypt'
    SECURITY_PASSWORD_SALT = '3nF3Rn0'

    SECURITY_POST_LOGIN_VIEW = '/dashboard'
    SECURITY_POST_CONFIRM_VIEW = '/dashboard'

    SESSION_TYPE = 'redis'
    SESSION_REDIS = redis.from_url(
        os.environ.get('SESSION_REDIS', 'redis://:{0}@localhost:6379/1'.format(os.environ.get('REDIS_PASS'))))
    PERMANENT_SESSION_LIFETIME = 3600

    # flask mail settings
    MAIL_SERVER = 'smtp.domain.com'
    MAIL_PORT = 465
    MAIL_USE_SSL = True
    MAIL_USERNAME = 'user'
    MAIL_PASSWORD = 'pass'
    SECURITY_EMAIL_SENDER = 'info@domain.com'


class ProdConfig(Config):
    ENV = 'prod'
    DEBUG = False
    DEBUG_TB_ENABLED = False  # Disable Debug toolbar


class DevConfig(Config):
    ENV = 'dev'
    DEBUG = True
    DEBUG_TB_ENABLED = True
    CACHE_TYPE = 'simple'  # Can be "memcached", "redis", etc.
