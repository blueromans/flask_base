# -*- coding: utf-8 -*-

from flask import Flask, render_template
from flask_security import Security, SQLAlchemyUserDatastore

from application.settings import ProdConfig
from application.user.forms import ExtendedRegisterForm
from application.user.models import User, Role
from application.extensions import cache, db, debug_toolbar, session
from application.public.views import bp_public
from application.user.views import bp_user
import application.commands as commands


def create_app(config_object=ProdConfig):
    app = Flask(__name__)
    app.config.from_object(config_object)
    register_extensions(app)
    register_blueprints(app)
    register_error_handlers(app)
    register_shell_context(app)
    register_commands(app)
    return app


def register_extensions(app):
    cache.init_app(app)
    db.init_app(app)
    user_datastore = SQLAlchemyUserDatastore(db, User, Role)
    Security(app, user_datastore, register_form=ExtendedRegisterForm)
    debug_toolbar.init_app(app)
    session.init_app(app)
    return None


def register_blueprints(app):
    app.register_blueprint(bp_public)
    app.register_blueprint(bp_user)
    return None


def register_error_handlers(app):
    def render_error(error):
        error_code = getattr(error, 'code', 500)
        return render_template("{0}.html".format(error_code)), error_code

    for errcode in [401, 404, 500]:
        app.errorhandler(errcode)(render_error)
    return None


def register_shell_context(app):
    def shell_context():
        return {
            'db': db,
            'User': User}

    app.shell_context_processor(shell_context)


def register_commands(app):
    app.cli.add_command(commands.clean)
    app.cli.add_command(commands.create_db)
    app.cli.add_command(commands.install)
    app.cli.add_command(commands.create)
    app.cli.add_command(commands.add_role)
    app.cli.add_command(commands.reset)
