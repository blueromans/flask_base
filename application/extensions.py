# -*- coding: utf-8 -*-
from flask_sqlalchemy import SQLAlchemy
from flask_caching import Cache
from flask_debugtoolbar import DebugToolbarExtension
from flask_migrate import Migrate
from flask_session import Session

db = SQLAlchemy()
cache = Cache()
debug_toolbar = DebugToolbarExtension()
migrate = Migrate()
session = Session()
